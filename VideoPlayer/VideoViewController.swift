//
//  VideoViewController.swift
//  VideoPlayer
//
//  Created by Dev's Mac on 18/01/21.
//  Copyright © 2021 Dev's Mac. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
import AVKit
import Photos
import MediaPlayer

class VideoViewController: UIViewController {

    @IBOutlet weak var videoView: UIView!
    var url : URL?
    var assetArray = [AVAsset]()
    var avplayer = AVPlayer()
    var playerController = AVPlayerViewController()
    var queuePlayer: AVQueuePlayer?
    var queue: [AVPlayerItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pyay()
        // Do any additional setup after loading the view.
    }
    
    func pyay() {
        OptiVideoEditor().transitionAnimation(videoUrl: url!, animation: true, type: 0, playerSize: self.videoView.frame, success: { (url) in
            DispatchQueue.main.async {
                self.addVideoPlayer(videoUrl: url, to: self.videoView)
            }
        }) { (error) in
            DispatchQueue.main.async {
//                OptiToast.showNegativeMessage(message: error ?? "")
//                self.progressvw_back.isHidden = true

            }
        }
    }
    
    func addVideoPlayer(videoUrl: URL, to view: UIView) {
        self.avplayer = AVPlayer(url: videoUrl)
        playerController.player = self.avplayer
        self.addChild(playerController)
        view.addSubview(playerController.view)
        playerController.view.frame = view.bounds
        playerController.showsPlaybackControls = true
        self.avplayer.play()
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
