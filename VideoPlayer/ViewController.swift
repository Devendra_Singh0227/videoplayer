//
//  ViewController.swift
//  VideoPlayer
//
//  Created by Dev's Mac on 30/12/20.
//  Copyright © 2020 Dev's Mac. All rights reserved.
//

import UIKit
import LongPressReorder
import Photos
import AVKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, AVPlayerViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var songsTableView: LongPressReorderTableView!
    var photos: PHFetchResult<PHAsset>!
    var queue: [AVPlayerItem] = []
    var queuePlayer: AVQueuePlayer?
    var playerViewController = AVPlayerViewController()
    @IBOutlet weak var songs_TableVIEW: UITableView!
    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    var playlists = [String]()
    @IBOutlet weak var playlistTxtField: UITextField!
    var imagePicker = UIImagePickerController()
    var Songimage = UIImage()
    var Url : URL?
    @IBOutlet weak var video_View: UIView!
    var avplayer = AVPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //getAssetFromPhoto()
        
         self.songs_TableVIEW.register(UINib(nibName: "MainTableViewCell", bundle: nil), forCellReuseIdentifier: "MainTableViewCell")
//        tableView.tableFooterView = UIView(frame: CGRect.zero)
//        songsTableView = LongPressReorderTableView(tableView, scrollBehaviour: .early)
//        songsTableView.delegate = self
//        songsTableView.enableLongPressReorder()
        // Do any additional setup after loading the view.
    }

    func getAssetFromPhoto() {
        let options = PHFetchOptions()
        options.sortDescriptors = [ NSSortDescriptor(key: "creationDate", ascending: true) ]
        options.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)
        photos = PHAsset.fetchAssets(with: options)
//        print("photos",photos)
        for i in 0..<photos.count {
           addVideos(videoAsset: photos[i])
        }
    }
    
    @IBAction func sideMenu(_ sender: UIBarButtonItem) {
        
    }
    
    @IBAction func add_Btn(_ sender: UIButton) {
        if sender.currentTitle == "Done"{
            sender.setTitle("Add Playlist", for: .normal)
            textView.isHidden = true
            textViewHeight.constant = 0
            playlists.append(playlistTxtField.text!)
            playlistTxtField.text = ""
//            self.Open_library()
            songs_TableVIEW.reloadData()
        } else {
            sender.setTitle("Done", for: .normal)
            textView.isHidden = false
            textViewHeight.constant = 64
        }
    }
    
    func addVideos (videoAsset: PHAsset) {

        guard (videoAsset.mediaType == .video) else {
            print("Not a valid video media type")
            return
        }

        PHCachingImageManager().requestAVAsset(forVideo: videoAsset, options: nil) { (asset, audioMix, args) in
            let asset = asset as! AVURLAsset
            self.queue.append(AVPlayerItem(url: NSURL(string: "\(asset.url)")! as URL))
        }
    }
    
    func Open_library() {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
    
     func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.mediaTypes = ["public.image", "public.movie"]
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
        
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL
        print(videoURL!)
        self.queue.append(AVPlayerItem(url: videoURL!))
        let image = videoPreviewImage(url: videoURL!)
        Songimage = image!
        Url = videoURL//"\(String(describing: videoURL))"
        imagePicker.dismiss(animated: true, completion: nil)
        self.songs_TableVIEW.reloadData()
//        self.playVideo()
    }
    
    func videoPreviewImage(url: URL) -> UIImage? {
        let asset = AVURLAsset(url: url)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        if let cgImage = try? generator.copyCGImage(at: CMTime(seconds: 2, preferredTimescale: 60), actualTime: nil) {
            return UIImage(cgImage: cgImage)
        }
        else {
            return nil
        }
    }
    
     func numberOfSections(in tableView: UITableView) -> Int {
        return playlists.count
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = songs_TableVIEW.dequeueReusableCell(withIdentifier: "MainTableViewCell", for: indexPath) as? MainTableViewCell
        cell?.asset = { (value) in
            //print(value)
            if value == "open" {
                self.Open_library()
            } else {
                self.playVideo()
            }
        }
        cell?.urlarray = { (value) in
           // print(value)
            self.queue.removeAll()
            self.setnewurl(value: value)
        }
        if self.queue.count > 0 {
            cell?.imageArray.append(Songimage)
            cell?.items1.append(Url!)
            cell?.songs_CollectionView.reloadData()
        }
        
//        let asset = photos!.object(at: indexPath.row)
//        let width: CGFloat = 100
//        let height: CGFloat = 100
//        let size = CGSize(width:width, height:height)
//        PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: PHImageContentMode.aspectFill, options: nil) { (image, userInfo) -> Void in
////            print(userInfo)
//            cell?.song_Image.image = image
//            cell?.time_Lbl.text = String(format: "%02d:%02d",Int((asset.duration / 60)),Int(asset.duration) % 60)
//        }
        return cell!
    }

    func setnewurl(value: [URL]) {
        for url in value {
             self.queue.append(AVPlayerItem(url: url))
        }
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // let asset = photos!.object(at: indexPath.row)
        //let width: CGFloat = 100
        //let height: CGFloat = 100
        //let size = CGSize(width:width, height:height)
        //PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: PHImageContentMode.aspectFill, options: nil) { (image, userInfo) -> Void in
//            cell?.song_Image.image = image
//            cell?.time_Lbl.text = String(format: "%02d:%02d",Int((asset.duration / 60)),Int(asset.duration) % 60)
        //}
        //playVideo(view: self, videoAsset: asset)
    }

     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }

     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 70))
        headerView.backgroundColor = .white
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: headerView.frame.size.width, height: headerView.frame.size.height))
        let headingLabel = UILabel(frame: CGRect(x: 10, y: 20, width: tableView.frame.width-20, height: 30))
        headingLabel.font = UIFont.boldSystemFont(ofSize: 20)
        headingLabel.textColor = .black
        headingLabel.text = playlists[section]
        headerView.addSubview(btn)
        headerView.addSubview(headingLabel)
        
        return headerView
    }

//    func playVideo (view: UIViewController, videoAsset: PHAsset) {
    func playVideo () {
        
        OptiVideoEditor().transitionAnimation(videoUrl: Url!, animation: true, type: -1, playerSize: self.view.frame, success: { (url) in
            DispatchQueue.main.async {
                self.video_View.isHidden = false
                self.addVideoPlayer(videoUrl: url, to: self.video_View)
            }
        }) { (error) in
            DispatchQueue.main.async {
//                OptiToast.showNegativeMessage(message: error ?? "")
//                self.progressvw_back.isHidden = true

            }
        }

//            DispatchQueue.main.async {
//                self.playerViewController.delegate = self
//                self.queuePlayer = AVQueuePlayer(items: self.queue)
//                avplayer = AVPlayer(url: asset.url)
//                self.playerViewController.player = self.queuePlayer
//                self.present(self.playerViewController, animated: true) {
//                    self.playerViewController.player!.play()
//                }
//            }
    }
    
    func addVideoPlayer(videoUrl: URL, to view: UIView) {
        self.avplayer = AVPlayer(url: videoUrl)
        playerViewController.player = self.avplayer
        self.addChild(playerViewController)
        view.addSubview(playerViewController.view)
        playerViewController.view.frame = view.bounds
        playerViewController.showsPlaybackControls = true
        self.avplayer.play()
    }
    
    func playerViewController(_ playerViewController: AVPlayerViewController, willEndFullScreenPresentationWithAnimationCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        self.queue.removeAll()
        self.photos = nil
        self.getAssetFromPhoto()
        self.songs_TableVIEW.reloadData()
    }
}

//extension ViewController {
//
//    override func startReorderingRow(atIndex indexPath: IndexPath) -> Bool {
//        return true
//    }
//
//    override func allowChangingRow(atIndex indexPath: IndexPath) -> Bool {
//        return true
//    }
//
//    override func positionChanged(currentIndex: IndexPath, newIndex: IndexPath) {
//        queue.swapAt(currentIndex.row, newIndex.row)
//    }
//}
 
