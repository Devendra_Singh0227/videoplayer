//
//  SideMenuViewController.swift
//  VideoPlayer
//
//  Created by Dev's Mac on 31/12/20.
//  Copyright © 2020 Dev's Mac. All rights reserved.
//

import UIKit

class Offers_Cell: UITableViewCell {
    
    static let identifier = "Offers_Cell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class SideMenuViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var sideMenuTableView: UITableView!
    var array = ["Songs", "PLaylist", "Profile", "Help", "Logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = sideMenuTableView.dequeueReusableCell(withIdentifier: "Offers_Cell", for: indexPath) as? Offers_Cell
        cell?.textLabel?.text = array[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

}
