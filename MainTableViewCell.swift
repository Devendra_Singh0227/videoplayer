//
//  MainTableViewCell.swift
//  VideoPlayer
//
//  Created by Dev's Mac on 12/01/21.
//  Copyright © 2021 Dev's Mac. All rights reserved.
//

import UIKit
import Photos
import AVKit

class MainTableViewCell: UITableViewCell, DrapDropCollectionViewDelegate {
    
    @IBOutlet weak var songs_CollectionView: DragDropCollectionView!
    var urlarray : (([URL]) -> ())?
    var items1 = [URL]()
    var asset:((String) -> ())?
    var imageArray = [UIImage]()//["none", "chrome", "fade", "falseColor", "instant", "mono", "noir", "process", "sepia", "tonal", "transfer"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.songs_CollectionView.delegate = self
        self.songs_CollectionView.dataSource = self
        
        songs_CollectionView.draggingDelegate = self
        songs_CollectionView.enableDragging(true)
        
        self.songs_CollectionView.register(UINib(nibName: "SongsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SongsCollectionViewCell")
        self.songs_CollectionView.register(UINib(nibName: "AddSongCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AddSongCollectionCell")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension MainTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items1.count + 1
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
         if indexPath.row != items1.count {
            let cell = songs_CollectionView.dequeueReusableCell(withReuseIdentifier: "SongsCollectionViewCell", for: indexPath) as? SongsCollectionViewCell
            cell?.Song_image.image = imageArray[indexPath.row]
            return cell!
        } else {
            let cell = songs_CollectionView.dequeueReusableCell(withReuseIdentifier: "AddSongCollectionCell", for: indexPath) as? AddSongCollectionCell
            return cell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        if indexPath.row != items1.count {
           asset?("play")
        } else {
            asset?("open")
        }        
    }
    
    func dragDropCollectionViewDidMoveCellFromInitialIndexPath(_ initialIndexPath: IndexPath, toNewIndexPath newIndexPath: IndexPath) {
         let imageToMove = items1[initialIndexPath.row]
        items1.remove(at: initialIndexPath.row)
        items1.insert(imageToMove, at: newIndexPath.row)
        urlarray?(items1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               
        return CGSize(width: 140, height: 200)
                      
       }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.1
    }
    
//    private func reorderItems(coordinator: UICollectionViewDropCoordinator, destinationIndexPath: IndexPath, collectionView: UICollectionView)
//       {
//           let items = coordinator.items
//           if items.count == 1, let item = items.first, let sourceIndexPath = item.sourceIndexPath
//           {
//               var dIndexPath = destinationIndexPath
//               if dIndexPath.row >= collectionView.numberOfItems(inSection: 0)
//               {
//                   dIndexPath.row = collectionView.numberOfItems(inSection: 0) - 1
//               }
//               collectionView.performBatchUpdates({
////                   if collectionView === self.collectionView2 {
//                       self.items1.remove(at: sourceIndexPath.row)
//                       self.items1.insert(item.dragItem.localObject as! String, at: dIndexPath.row)
////                   }
//                   collectionView.deleteItems(at: [sourceIndexPath])
//                   collectionView.insertItems(at: [dIndexPath])
//               })
//               coordinator.drop(items.first!.dragItem, toItemAt: dIndexPath)
//           }
//       }
//
//    private func copyItems(coordinator: UICollectionViewDropCoordinator, destinationIndexPath: IndexPath, collectionView: UICollectionView)
//    {
//        collectionView.performBatchUpdates({
//            var indexPaths = [IndexPath]()
//            for (index, item) in coordinator.items.enumerated()
//            {
//                let indexPath = IndexPath(row: destinationIndexPath.row + index, section: destinationIndexPath.section)
//
//                    self.items1.insert(item.dragItem.localObject as! String, at: indexPath.row)
//
//                indexPaths.append(indexPath)
//            }
//            collectionView.insertItems(at: indexPaths)
//        })
//    }
    
}

//extension MainTableViewCell : UICollectionViewDragDelegate
//{
//    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem]
//    {
//        let item = self.items1[indexPath.row]
//        let itemProvider = NSItemProvider(object: item as NSString)
//        let dragItem = UIDragItem(itemProvider: itemProvider)
//        dragItem.localObject = item
//        return [dragItem]
//    }
//
//    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem]
//    {
//        let item = self.items1[indexPath.row]
//        let itemProvider = NSItemProvider(object: item as NSString)
//        let dragItem = UIDragItem(itemProvider: itemProvider)
//        dragItem.localObject = item
//        return [dragItem]
//    }
//
//    func collectionView(_ collectionView: UICollectionView, dragPreviewParametersForItemAt indexPath: IndexPath) -> UIDragPreviewParameters?
//    {
//        if collectionView == songs_CollectionView
//        {
//            let previewParameters = UIDragPreviewParameters()
//            previewParameters.visiblePath = UIBezierPath(rect: CGRect(x: 25, y: 25, width: 120, height: 120))
//            return previewParameters
//        }
//        return nil
//    }
//}
//
//// MARK: - UICollectionViewDropDelegate Methods
//extension MainTableViewCell : UICollectionViewDropDelegate
//{
//    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool
//    {
//        return session.canLoadObjects(ofClass: NSString.self)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal
//    {
//        if collectionView === self.songs_CollectionView
//        {
//            if collectionView.hasActiveDrag
//            {
//                return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
//            }
//            else
//            {
//                return UICollectionViewDropProposal(operation: .forbidden)
//            }
//        }
//        else
//        {
//            if collectionView.hasActiveDrag
//            {
//                return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
//            }
//            else
//            {
//                return UICollectionViewDropProposal(operation: .copy, intent: .insertAtDestinationIndexPath)
//            }
//        }
//    }
//
//    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator)
//    {
//        let destinationIndexPath: IndexPath
//        if let indexPath = coordinator.destinationIndexPath
//        {
//            destinationIndexPath = indexPath
//        }
//        else
//        {
//            // Get last index path of table view.
//            let section = collectionView.numberOfSections - 1
//            let row = collectionView.numberOfItems(inSection: section)
//            destinationIndexPath = IndexPath(row: row, section: section)
//        }
//
//        switch coordinator.proposal.operation
//        {
//        case .move:
//            self.reorderItems(coordinator: coordinator, destinationIndexPath:destinationIndexPath, collectionView: collectionView)
//            break
//
//        case .copy:
//            self.copyItems(coordinator: coordinator, destinationIndexPath: destinationIndexPath, collectionView: collectionView)
//
//        default:
//            return
//        }
//    }
//}
